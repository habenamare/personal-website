+++
title = "Haben Amare"
profileImage = "/images/haben-750.jpg"
shortDescription = "I build products with clean code, and find great joy in the process."

[[socialNetworks]]
  title = "Find me on Github"
  link = "https://github.com/habenamare"
[[socialNetworks]]
  title = "Find me on GitLab"
  link =  "https://gitlab.com/habenamare"

[[skills]]
  skill = "Front-end"
  using = [ "VueJS" ]
[[skills]]
  skill = "Back-end, APIs"
  using = [ "NodeJS", "Laravel", "Go" ]
[[skills]]
  skill = "Databases, Database Design"
  using = [ "PostgreSQL", "MongoDB", "MySQL" ]
[[skills]]
  skill = "Cloud"
  using = [ "Firebase" ]
+++

{{< selfdescription >}}
Hello,

I am a **self-taught** software developer, and I also have a Bachelor of Science Degree in **Computer Science** from *Addis Ababa University*. I am **passionate** about software development and its related fields. I **enjoy learning** new technologies and I try my best to **keep myself up to date** with the latest technologies. Currently, I am mainly interested in **web technologies**.
{{< /selfdescription >}}

{{< skills >}}
